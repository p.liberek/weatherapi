from datetime import datetime
import requests
import sys


class WeatherForecast:

    BASE_URL = 'http://api.weatherapi.com/v1/history.json'

    def __init__(self, api_key, date=str(datetime.today().date())):
        self.api_key = api_key
        self.date = date
        self.data = self.get_data()

    def get_data(self):
        request_url = f'{self.BASE_URL}?key={self.api_key}&q=Warsaw&dt={self.date}'
        r = requests.get(request_url)
        content = r.json()
        return content

    def get_avg_temperature(self):
        avg_temp = self.data['forecast']['forecastday'][0]['day']['avgtemp_c']
        return avg_temp

    def get_min_temperature(self):
        min_temp = self.data['forecast']['forecastday'][0]['day']['mintemp_c']
        return min_temp

    def get_max_temperature(self):
        max_temp = self.data['forecast']['forecastday'][0]['day']['maxtemp_c']
        return max_temp

    def get_rain_info(self):
        totalprecip_mm = float(self.data['forecast']['forecastday'][0]['day']['totalprecip_mm'])
        return self.get_rain_chance(totalprecip_mm)
    
    def get_max_wind_speed(self):
        max_wind_speed = self.data['forecast']['forecastday'][0]['day']['maxwind_kph']
        return max_wind_speed

    def get_avg_humidity(self):
        avg_humidity = self.data['forecast']['forecastday'][0]['day']['avghumidity']
        return avg_humidity

    def get_rain_chance(self, totalprecip_mm):
        if totalprecip_mm > 0.0:
            return "Będzie padać"
        elif totalprecip_mm == 0.0:
            return "Nie będzie padać"
        return "Nie wiem!"

weather = WeatherForecast(api_key=sys.argv[1], date=sys.argv[2])
print(f'W dniu: {weather.date}')
print(f'Szansa na opady: {weather.get_rain_info()}')
print(f'Minimalna temperatura wynosi: {weather.get_min_temperature()} stopni Celsjusza')
print(f'Średnia temperatura wynosi: {weather.get_avg_temperature()} stopni Celsjusza')
print(f'Maksymalna temperatura wynosi: {weather.get_max_temperature()} stopni Celsjusza')
print(f'Średnia wilgotność wynosi: {weather.get_avg_humidity()}%')
print(f'Maksymalna prędkość wiatru wynosi: {weather.get_max_wind_speed()} km/h')
